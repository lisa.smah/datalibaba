#' commenter_table : ajouter (ou remplacer) un commentaire de table dans le SGBD
#'
#' @param comment : Le texte du commentaire a poster.
#' @param table : Le texte du commentaire a poster.
#' @param schema : Le nom du schema qui contient la table a commenter.
#' @param db : Le nom de la base qui contient la table a commenter.
#' @param user : L'identifiant de l'utilisateur SGBD qui se connecte.
#'
#' @importFrom attempt stop_if stop_if_not
#' @importFrom DBI dbDisconnect
#' @importFrom glue glue
#' @importFrom rpostgis dbComment
#' @return NULL
#' @export
#'
#' @examples
#' \dontrun{
#' commenter_table(comment = "Je suis un commentaire important.", table = "test_iris",
#'                 schema = "public", db  = "public", user = "does")
#' }

commenter_table <- function(comment = NULL, table = NULL, schema = NULL, db  = "public", user = "does") {

  # ouverture de la connexion avec DBI::dbDriver("PostgreSQL")
  con <- connect_to_db(db, user)

  # verification de la validite des arguments
  attempt::stop_if(comment, is.null, msg = "L'argument comment n\'est pas renseign\u00e9. ")
  attempt::stop_if(schema, is.null, msg = "L'argument schema n\'est pas renseign\u00e9. ")
  attempt::stop_if(table, is.null, msg = "L'argument table n\'est pas renseign\u00e9. ")
  attempt::stop_if_not(table %in% list_tables(con, schema), msg = glue::glue("Il n'y a pas de table {table} dans le schema {schema}. "))

  meta <- paste0("\nCommentaire post\u00e9 le ", format(Sys.Date(), '%d/%m/%Y'), ", par ", Sys.getenv("USERNAME"), ".")

  # securisation encodage des accents et caracteres speciaux
  comment2 <- paste0(enc2utf8(comment), enc2utf8(meta))

  rpostgis::dbComment(conn = con, name = c(schema, table), comment = comment2, type = "table", exec = TRUE)

  DBI::dbDisconnect(con)

  return(invisible(NULL))
}



#' commenter_schema : ajouter (ou remplacer) un commentaire pour un schema du SGBD
#'
#' @param comment : le texte du commentaire a poster
#' @param schema : le nom du schema a commenter
#' @param db : le nom de la base qui contient le schema a commenter
#' @param user : l'identifiant de l'utilisateur SGBD qui se connecte
#' @importFrom attempt stop_if
#' @importFrom DBI dbDisconnect
#' @importFrom rpostgis dbComment
#' @return NULL
#' @export
commenter_schema <- function(comment = NULL, schema = NULL, db  = "public", user = "does") {

  # ouverture de la connexion avec DBI::dbDriver("PostgreSQL")
  con <- connect_to_db(db, user)

  # verification de la validite des arguments
  attempt::stop_if(comment, is.null, msg = "L'argument comment n\'est pas renseign\u00e9. ")
  attempt::stop_if(schema, is.null, msg = "L'argument schema n\'est pas renseign\u00e9. ")

  comment2 <- enc2utf8(comment)

  rpostgis::dbComment(conn = con, name = schema, comment = comment2, type = "schema", exec = TRUE)

  DBI::dbDisconnect(con)

  return(invisible(NULL))
}

